#include "image_structure.h"
#include <stddef.h>

typedef void (*rotate_pixels)(const struct image* const source_image, struct image* const result_image);

static void rotate_pixels90(const struct image* const source_image, struct image* const result_image) {
    if (result_image->pixels == NULL) return;
    for (uint64_t w = 0; w < source_image->width; w++) {
        for (uint64_t h = 0; h < source_image->height; h++) {
            struct pixel pixel = get_pixel(source_image, w, h);
            set_pixel(result_image, h, source_image->width - 1 - w, pixel);
        }
    }
}

static void rotate_pixels180(const struct image* const source_image, struct image* const result_image) {
    if (result_image->pixels == NULL) return;
    for (uint64_t w = 0; w < source_image->width; w++) {
        for (uint64_t h = 0; h < source_image->height; h++) {
            struct pixel pixel = get_pixel(source_image, w, h);
            set_pixel(result_image, source_image->width - 1 - w, source_image->height - 1 - h, pixel);
        }
    }
}

static void rotate_pixels270(const struct image* const source_image, struct image* const result_image) {
    if (result_image->pixels == NULL) return;
    for (uint64_t w = 0; w < source_image->width; w++) {
        for (uint64_t h = 0; h < source_image->height; h++) {
            struct pixel pixel = get_pixel(source_image, w, h);
            set_pixel(result_image, source_image->height - 1 - h, w, pixel);
        }
    }
}

const rotate_pixels rotators[] = {
        rotate_pixels90,
        rotate_pixels180,
        rotate_pixels270
};

struct image get_rotated_image(const struct image* const source_image, const int64_t angle) {
    if (angle == 0) return copy_image(source_image);
    struct image result_image =
            angle == 180 ? create_new_image(source_image->width, source_image->height)
            : create_new_image(source_image-> height, source_image->width);
    rotators[angle / 90 - 1](source_image, &result_image);
    return result_image;
}
