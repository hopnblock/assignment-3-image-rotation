#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#define ARG_COUNT 3

bool check_arg_count(const int argc) {
    return argc == ARG_COUNT + 1;
}

static int64_t try_parse_angle(const char* angle) {
    char* end_ptr;
    const int64_t parsed = strtol(angle, &end_ptr, 10);
    if (end_ptr == angle) return -1;
    return parsed;
}

int64_t try_get_angle(const char* angle) {
    const int64_t angle_value = try_parse_angle(angle);
    if (angle_value == -1) return -1;
    if (llabs(angle_value) % 90 != 0) return -1;
    if (llabs(angle_value) > 270) return -1;
    return angle_value >= 0 ? angle_value : 360 + angle_value;
}
