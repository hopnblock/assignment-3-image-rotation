#include <stdint.h>

struct pixel {
    uint8_t b;
    uint8_t g;
    uint8_t r;
};

struct image {
    uint64_t width;
    uint64_t height;
    struct pixel* pixels;
};

struct image create_new_image(uint64_t width, uint64_t height);

struct pixel get_pixel(struct image const* image, uint64_t x, uint64_t y);

void set_pixel(struct image* image, uint64_t x, uint64_t y, struct pixel pixel);

void free_image_pixels(struct image* image);

struct image copy_image(const struct image* source_image);
