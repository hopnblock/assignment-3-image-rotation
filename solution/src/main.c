#include "arg_validator.h"
#include "bmp_structure.h"
#include "printer.h"
#include "transformer.h"
#include <stdio.h>

//error codes for more convenient error printing
enum errors {
    ARG_COUNT = 0,
    ANGLE,
    SOURCE_OPEN,
    BMP_READ,
    ALLOCATION,
    RESULT_OPEN,
    ROTATED_ALLOCATION,
    BMP_WRITE
};

int main(int argc, char* argv[]) {

    //check argument count
    if (!check_arg_count(argc)) {
        print_error(ARG_COUNT);
        return 1;
    }

    //check angle validity
    int64_t angle = try_get_angle(argv[3]);
    if (angle < 0) {
        print_error(ANGLE);
        return 1;
    }

    //try to open source file
    char* source_file_name = argv[1];
    FILE* source_file = fopen(source_file_name, "rb");
    if (source_file == NULL) {
        print_error(SOURCE_OPEN);
        return 1;
    }

    //try to read image from source file and allocate memory for source image
    struct image source_image;
    int reading_result = read_source_image(source_file, &source_image);
    fclose(source_file);
    if (reading_result != 0) {
        reading_result > 0 ? print_error(BMP_READ) : print_error(ALLOCATION);
        free_image_pixels(&source_image);
        return 1;
    }

    //try to open result file
    char* result_file_name = argv[2];
    FILE* result_file = fopen(result_file_name, "wb");
    if (result_file == NULL) {
        print_error(RESULT_OPEN);
        return 1;
    }

    //get a new rotated image
    struct image result_image = get_rotated_image(&source_image, angle);
    if (result_image.pixels == NULL) {
        print_error(ROTATED_ALLOCATION);
        return 1;
    }

    //try to write to result file
    bool writing_result = write_result_image(result_file, &result_image);
    fclose(result_file);
    free_image_pixels(&source_image);
    free_image_pixels(&result_image);
    if (!writing_result) {
        print_error(BMP_WRITE);
        return 1;
    }

    //all good :)
    print_rotation_success(argv);
    return 0;

}
