#include <stdio.h>

const char* error_messages[] = {
        "Error: image_transformer requires 3 arguments - <source_file>, <result_file>, <angle>",
        "Error: wrong angle argument. Please, use one of the following: [0, 90, -90, 180, -180, 270, -270]",
        "Error: couldn't open the source file in read mode",
        "Error: failed to read the source image or it has incorrect format",
        "Error: couldn't allocate memory for the source image's pixels",
        "Error: couldn't open the result file in write mode",
        "Error: couldn't allocate memory for the rotated image",
        "Error: couldn't save the transformed image to the result file"
};

static void print_error_message(const char* err_message) {
    fprintf(stderr, "%s\n", err_message);
}

void print_error(const int error_index) {
    print_error_message(error_messages[error_index]);
}

void print_rotation_success(char* args[]) {
    printf("Successfully rotated image %s by %s degrees and saved to %s\n", args[1], args[3], args[2]);
}
