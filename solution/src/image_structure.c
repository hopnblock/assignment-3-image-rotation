#include "image_structure.h"
#include <stdlib.h>
#include <string.h>

struct image create_new_image(const uint64_t width, const uint64_t height) {
    struct pixel* pixels = malloc(width * height * sizeof (struct pixel));
    return (struct image) {width, height, pixels};
}

struct pixel get_pixel(struct image const* const image, const uint64_t x, const uint64_t y) {
    return image->pixels[x + y * image->width];
}

void set_pixel(struct image* const image, const uint64_t x, const uint64_t y, const struct pixel pixel) {
    image->pixels[x + y * image->width] = pixel;
}

void free_image_pixels(struct image* const image) {
    if (image->pixels != NULL) {
        free(image->pixels);
        image->pixels = NULL;
    }
}

struct image copy_image(const struct image* const source_image) {
    struct image copy = create_new_image(source_image->width, source_image->height);
    if (copy.pixels == NULL) return copy;
    memcpy(copy.pixels, source_image->pixels, copy.width * copy.height * sizeof (struct pixel));
    return copy;
}
