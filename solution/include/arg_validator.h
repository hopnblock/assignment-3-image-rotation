#include <stdbool.h>
#include <stdint.h>

bool check_arg_count(int argc);

int32_t try_get_angle(const char* angle);
