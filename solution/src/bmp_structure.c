#include "image_structure.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>

#define TYPE 0x4D42
#define RESERVED 0
#define SIZE 40
#define PLANES 1
#define BIT_COUNT 24
#define COMPRESSION_ONE 0
#define COMPRESSION_TWO 3
#define X_PELS_PER_METER 1
#define Y_PELS_PER_METER 1
#define CLR_USED 0
#define CLR_IMPORTANT 0

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

static bool validate_bmp_header(const struct bmp_header* header) {
    return (header->bfType == TYPE) && (header->biBitCount == BIT_COUNT)
           && (header->bfileSize > sizeof(struct bmp_header))
           && (header->bOffBits == sizeof(struct bmp_header))
           && (header->biCompression == COMPRESSION_ONE || header->biCompression == COMPRESSION_TWO)
           && (header->biSizeImage == header->bfileSize - header->bOffBits)
           && (header->biWidth > 0) && (header->biHeight > 0)
           && (header->biPlanes == PLANES)
           && (header->biXPelsPerMeter >= 0) && (header->biYPelsPerMeter >= 0);
}

static bool read_bmp_header(FILE* source_file, struct bmp_header* header) {
    if (!fread(header, sizeof(struct bmp_header), 1, source_file)) return false;
    return validate_bmp_header(header);
}

static uint8_t calculate_padding(const uint64_t width) {
    return 4 - (width * sizeof (struct pixel)) % 4;
}

int read_source_image(FILE* source_file, struct image* const image) {
    struct bmp_header header = {0};
    if (!read_bmp_header(source_file, &header)) return 1;
    *image = create_new_image(header.biWidth, header.biHeight);
    if (image->pixels == NULL) return -1;
    const uint8_t padding = calculate_padding(header.biWidth);
    for (uint64_t i = 0; i < header.biHeight; i++) {
        if (!fread(image->pixels + header.biWidth * i,
                   sizeof (struct pixel), header.biWidth, source_file)) {
            return 1;
        }
        if (fseek(source_file, padding, SEEK_CUR)) {
            return 1;
        }
    }
    return 0;
}

static struct bmp_header create_bmp_header(const struct image* const image) {
    uint8_t padding = calculate_padding(image->width);
    return (struct bmp_header) {
            TYPE,
            sizeof(struct bmp_header) +
            sizeof(struct pixel) * (image->width + padding) * image->height,
            RESERVED,
            sizeof(struct bmp_header),
            SIZE,
            image->width,
            image->height,
            PLANES,
            BIT_COUNT,
            COMPRESSION_ONE,
            image->height * (image->width + padding),
            X_PELS_PER_METER,
            Y_PELS_PER_METER,
            CLR_USED,
            CLR_IMPORTANT
    };
}

bool write_result_image(FILE* result_file, const struct image* const image) {
    uint8_t padding = calculate_padding(image->width);
    struct bmp_header header = create_bmp_header(image);
    if (!fwrite(&header, sizeof (struct bmp_header), 1, result_file)) return false;
    for (uint64_t h = 0; h < image->height; h++) {
        if (fwrite(image->pixels + image->width * h,
                   sizeof (struct pixel), image->width, result_file) != image->width)
            return false;
        if (fseek(result_file, padding, SEEK_CUR)) return false;
    }
    return true;
}
