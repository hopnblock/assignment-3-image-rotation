#include "image_structure.h"
#include <stdbool.h>
#include <stdio.h>

int read_source_image(FILE* source_file, struct image* image);

bool write_result_image(FILE* result_file, const struct image* image);
